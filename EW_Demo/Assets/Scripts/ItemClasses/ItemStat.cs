﻿using UnityEngine;
using System.Collections;
using EW.CharacterClasses.Stats;
using System;

namespace EW.ItemClasses
{
    [Serializable]
    public class ItemStat
    {
        [ReadOnly]
        public string statName;
        public CharacterStat characterStatType;//get from ActionActivationPhase?
        public int statValue = 0;
        public StatModifierType _statModifierType;//enum?
        public StatTarget target;
        public string durationType;//enum?
        public ActionActivationPhase statActionPhase;
        [ReadOnly]
        [TextArea]
        public string description;
        [TextArea]
        public string customDescription;

        public bool IsApplied { get; set; }

        internal void GenerateDescription()
        {
            statName = "";
            description = "";

            if (characterStatType != null)
            {
                statName = characterStatType._displayName;

                description = "";
                if (statActionPhase.actionPhase != null)
                {    
                    statActionPhase.phaseName = statActionPhase.actionPhase.name;
                    if (statActionPhase.actionPhase.GetType() != typeof(OnEquip))
                        description += string.Format("{0} ", statActionPhase.phaseName);
                    if(statActionPhase.isActivatedOnChance)
                    {
                        description += string.Format("{0}% for ", statActionPhase.activationChance);
                    }
                }

                description += string.Format("{0}{1}{3} {2}", statValue>0?"+":"-", statValue, statName, _statModifierType == StatModifierType.Percentage?"%":"");

            }
        }
    }

    public enum StatTarget
    {
        Self,
        Opponent
    }

    public enum StatModifierType
    {
        BaseValue,
        Percentage
    }

}