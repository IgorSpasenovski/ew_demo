﻿using UnityEngine;
using System.Collections;
using EW.CharacterClasses;
using EW.CharacterClasses.BodyParts;
using EW.ItemClasses;
using UnityEngine.UI;
using EW.CharacterClasses.Stats;
using EW.DuelCombatClasses;
using EW.PlayerClasses;
using EW.UIClasses.CharacterUI;
using EW.UIClasses.ItemsUI;
using EW.UIClasses.DuelUI;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class TestController : MonoBehaviour
{
    public Character characterTemplate;

    [HideInInspector]
    public Character myCharacter;
    private Character opponentCharacter;

    public Item someItem;
    public Item someChest;
    public Item bracers;
    public Item gloves;
    public WeaponItem sword;
    public WeaponItem shield;
    public WeaponItem longsword;

    public Text t_statsLog, t_statsOpponentLog;

    DuelCombatController duelController;
    Player player1, player2;

    [Space]
    public CharacterDisplay _characterDisplay;
    public CharacterDisplay _opponentCharacterDisplay;

    [Header("Buttons")]
    public Button b_nextAction;
    public Button b_selectRandomTarget;
    public Button b_rollDice;

    public Inventory _playerInventory;
    public Inventory _opponentInventory;

    public DiceRollDisplay _diceRollDisplay;

    [Header("Duel End")]
    public GameObject _EndPanel;
    public TextMeshProUGUI _endPanelMessage;
    public Button _playAgain;

    [Header("Bots")]
    private AutoBot _player1Bot;
    private AutoBot _player2Bot;
    public Toggle _player1BotToggle;
    public Toggle _player2BotToggle;

    [Header("Duel Timer")]
    public TextMeshProUGUI _duelTimer;
    private DateTime _startTime;

    private void Start()
    {
        if(myCharacter == null)
        {
            myCharacter = Instantiate(characterTemplate);
            myCharacter.InitCharacter();
        }

        

        opponentCharacter = Instantiate(characterTemplate);
        opponentCharacter.InitCharacter();

        duelController = new DuelCombatController();
        duelController.PrepareDuel(player1 = new Player("Homer", myCharacter, _diceRollDisplay), player2 = new Player("Bart", opponentCharacter, _diceRollDisplay));
        duelController.EndDuel = DisplayEndPanel;

        _diceRollDisplay.Init(duelController.RollDice);

        player1.PlayerHexColor = "#"+ColorUtility.ToHtmlStringRGB(Color.cyan);
        player2.PlayerHexColor = "#"+ColorUtility.ToHtmlStringRGB(Color.red);
        CombatLog.Instance.RegisterLoggerLineColor(player1.playerName, player1.PlayerHexColor);
        CombatLog.Instance.RegisterLoggerLineColor(player2.playerName, player2.PlayerHexColor);

        b_nextAction.onClick.AddListener(NextAction);
        b_selectRandomTarget.onClick.AddListener(SelectRandomTarget);
        b_rollDice.onClick.AddListener(RollDice);

        _characterDisplay.Init(myCharacter, _playerInventory);
        _opponentCharacterDisplay.Init(opponentCharacter, _opponentInventory);

        //set bots
        _player1Bot = new AutoBot(player1, player2, false);
        _player2Bot = new AutoBot(player2, player1, true);

        _player1BotToggle.onValueChanged.AddListener((b) => _player1Bot.IsActive = b);
        _player2BotToggle.onValueChanged.AddListener((b) => _player2Bot.IsActive = b);

        _startTime = DateTime.Now;
    }

    private string GetCharacterStats(Player pl)
    {
        string statsOutput = "";

        statsOutput += string.Format("<color={3}>Player {0} </color> - <color={1}> phase: {2} </color>  \n", pl.playerName, pl.IsActivePlayer?"green":"red", pl.PendingPhase, pl.PlayerHexColor);
        statsOutput += string.Format("<color={0}>", pl.PlayerHexColor);
        foreach (CharacterStat cStat in pl.PlayerCharacter.characterStats)
        {
            statsOutput += string.Format("{0} : {1} \n", cStat.name, cStat.Value);
        }

        statsOutput += "\nBODY PARTS \n";
        foreach (BodyPart bp in pl.PlayerCharacter.bodyParts)
        {
            statsOutput += string.Format("{0} : HP-{1}/{2} D-{3} ITEM-{4} \n", bp.displayName, bp.HealthPoints, bp.TotalHealthPoints, bp.GetStat<BodyPartDefense>().Value, bp.EquippedItem != null ? bp.EquippedItem.itemName : "none");
            if(bp.GetType() == typeof(Hand))
            {
                Hand hand = (Hand)bp;
                statsOutput += string.Format("{0} : {1} \n", hand._displayWeaponSlotName, hand.EquippedWeapon != null ? hand.EquippedWeapon.itemName : "none");
            }
        }
        statsOutput += "</color>";

        return statsOutput;

    }

    private void Update()
    {
        //if(Input.GetKeyDown(KeyCode.Space))
        //{
        //    //myCharacter.ModifyStat();
        //    //myCharacter.ActivatePhase<OnAttack>();
        //    myCharacter.EquipItem(someItem);
        //    myCharacter.EquipItem(someChest);
        //    myCharacter.EquipItem(bracers);
        //    myCharacter.EquipItem(gloves);
        //    myCharacter.EquipItem(sword);
        //    myCharacter.EquipItem(shield);

        //    //opponentCharacter.EquipItem(someItem);
        //    //opponentCharacter.EquipItem(someChest);
        //    //opponentCharacter.EquipItem(bracers);
        //    //opponentCharacter.EquipItem(longsword);
        //}

        //if(Input.GetKeyDown(KeyCode.Mouse1))
        //{
        //    myCharacter.UnequipItem(someItem);
        //    myCharacter.UnequipItem(someChest);
        //    myCharacter.UnequipItem(bracers);
        //    myCharacter.UnequipItem(gloves);
        //    myCharacter.EquipItem(longsword);
        //}

        //t_statsLog.text = GetCharacterStats(player1);
        //t_statsOpponentLog.text = GetCharacterStats(player2);
        

        duelController.CheckTurnPhaseExecution();

        TimerUpdate();
    }

    private void TimerUpdate()
    {
        TimeSpan passed = DateTime.Now - _startTime;
        _duelTimer.text = string.Format("{0:mm\\:ss}", passed);
    }

    private void NextAction()
    {
        player1.PlayerTestAction();
        player2.PlayerTestAction();
    }

    private void SelectRandomTarget()
    {
        player1.SelectBodyPartTarget(player2.PlayerCharacter.bodyParts[Random.Range(0, player2.PlayerCharacter.bodyParts.Count)]);
        //player2.SelectAttackTarget(player1.PlayerCharacter.bodyParts[Random.Range(0, player1.PlayerCharacter.bodyParts.Count)]);
        player2.SelectBodyPartTarget(player1.PlayerCharacter.bodyParts.Find(x => x.GetType() == typeof(EW.CharacterClasses.BodyParts.Chest)));
    }

    private void RollDice()
    {
        player1.RollDice();
        player2.RollDice();
    }

    public void DisplayEndPanel(string winnerName)
    {
        _EndPanel.SetActive(true);
        _endPanelMessage.text = winnerName + " has won the duel.";
        _playAgain.onClick.AddListener(() => SceneManager.LoadScene("EW_DemoScene"));
    }

    private void OnDestroy()
    {
        myCharacter.DestroyCharacter();
        opponentCharacter.DestroyCharacter();
        Debug.Log("destroyed test controller");
    }
}
