﻿using UnityEngine;
using EW.ItemClasses;
using EW.CharacterClasses.BodyParts;
using UnityEngine.EventSystems;

namespace EW.UIClasses.ItemsUI
{
    public class InventoryDisplay : MonoBehaviour, IDropHandler
    {
        [SerializeField] private RectTransform _contentParent;
        [SerializeField] private ItemInventoryDisplay _itemInventoryPrefab;
        [SerializeField] private ItemDisplay _itemDisplay;
        [SerializeField] private ItemInventoryDisplay _draggedItemInventoryDisplay;
        public ItemInventoryDisplay DraggedItemInventoryDisplay { get => _draggedItemInventoryDisplay; }

        public delegate void HighlightFromItem(Item targetItem, bool itemSelected);
        private HighlightFromItem OnItemSelect;
        private OnEquippedItem EquipItemCallback;
        private OnEquippedItem UnequipItemCallback;

        private ItemInventoryDisplay id;
        public void Init(Inventory i, HighlightFromItem itemSelectCallback, OnEquippedItem equipItemCallback, OnEquippedItem unequipItemCallback)
        {
            _itemDisplay.gameObject.SetActive(false);
            _draggedItemInventoryDisplay.gameObject.SetActive(false);

            EquipItemCallback = equipItemCallback;
            UnequipItemCallback = unequipItemCallback;

            
            foreach (Item item in i._items)
            {
                CreateItemInventoryDisplay(item);
            }

            OnItemSelect = itemSelectCallback;
        }

        public void CreateItemInventoryDisplay(Item item)
        {
            Item itemClone = Instantiate(item);// TODO: optimize not to instantiate SOs
            id = Instantiate(_itemInventoryPrefab, _contentParent);
            id.Init(itemClone, OnInventoryItemPointerEnter, OnInventoryItemPointerExit, _draggedItemInventoryDisplay, EquipItemCallback, UnequipItemCallback);
        }

        public void OnInventoryItemPointerEnter(Item i)
        {
            if (i != null)
            {
                _itemDisplay.DisplayItem(i);
                OnItemSelect?.Invoke(i, true);
            }
        }

        public void OnInventoryItemPointerExit(Item i)
        {
            _itemDisplay.Hide();

            if (i != null)
                OnItemSelect?.Invoke(i, false);
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag != null)
            {
                ItemInventoryDisplay invDisp = eventData.pointerDrag.GetComponent<ItemInventoryDisplay>();
                if (invDisp)
                {
                    if (invDisp.CurrentItem)
                    {
                        if (invDisp.CurrentItem.IsEquipped)
                        {
                            invDisp.UnequipItem();
                        }
                    }
                }
            }
        }
    }
}
