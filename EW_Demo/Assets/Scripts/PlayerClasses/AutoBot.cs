﻿
using EW.DuelCombatClasses;
using System.Threading.Tasks;
using UnityEngine;

namespace EW.PlayerClasses
{
    public class AutoBot
    {
        Player _playerToControll;
        Player _opponentPlayer;
        private bool _isActive = false;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if(_isActive == value)
                    return;

                _isActive = value;

                if(_isActive)
                    _playerToControll.OnPendingPhaseChange += OnPendingPhaseChange;
                else
                    _playerToControll.OnPendingPhaseChange -= OnPendingPhaseChange;

                OnPendingPhaseChange(_playerToControll.PendingPhase);
            }
        }
        public AutoBot(Player playerToControll, Player opponentPlayer, bool isActive)
        {
            _playerToControll = playerToControll;
            
            _opponentPlayer = opponentPlayer;

            IsActive = isActive;
            
        }

        private void OnPendingPhaseChange(PlayerTurnPhase _pendingPhase)
        {

            switch (_pendingPhase)
            {
                case PlayerTurnPhase.TurnStarted:
                    break;
                case PlayerTurnPhase.PlayerActions:
                    SelectRandomTarget();
                    break;
                case PlayerTurnPhase.RollDice:
                    _playerToControll.RollDice();
                    break;
                case PlayerTurnPhase.ProcessAttack:
                    break;
                case PlayerTurnPhase.ProcessDefense:
                    break;
                case PlayerTurnPhase.EndTurn:
                    break;
                default:
                    break;
            }
        }

        private void SelectRandomTarget()
        {
            _playerToControll.SelectBodyPartTarget(_opponentPlayer.PlayerCharacter.bodyParts[Random.Range(0, _opponentPlayer.PlayerCharacter.bodyParts.Count)]);
        }
    }
}