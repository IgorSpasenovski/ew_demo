﻿using UnityEngine;
using System.Collections;
using EW.CharacterClasses;
using EW.CharacterClasses.Stats;
using System.Collections.Generic;
using EW.CharacterClasses.BodyParts;
using TMPro;
using EW.UIClasses.ItemsUI;
using EW.ItemClasses;

namespace EW.UIClasses.CharacterUI
{
    //total health
    //stats display
    //body parts display
    //weapons display
    //phase display
    //combat/action log?
    public class CharacterDisplay : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _tPlayerName;

        [Header("Rect Containers")]
        [SerializeField]
        private RectTransform _characterStatContainer;
        [SerializeField]
        private RectTransform _bodyPartStatDisplayContainer;
        [SerializeField]
        private InventoryDisplay _inventoryDisplay;

        [Header("Prefabs")]
        [SerializeField]
        private StatDisplay _statDisplayPrefab;
        [SerializeField]
        private BodyPartStatsDisplay _bodyPartStatsDisplayPrefab;
        [SerializeField]
        private BodyPartStatsDisplay _handStatsDisplayPrefab;

        private List<BodyPartDisplay> _bodyPartDisplays;
        private List<BodyPartStatsDisplay> _bodyPartStatsDisplays;

        private Character _character;

        public void Init(Character character, Inventory _playerInventory)
        {
            _character = character;

            SetPlayerNameAndTotalHealthPoints();

            InitBodyPartDisplays();

            CreateStatsDisplays();

            AddHealthPointsChangeListeners();

            _inventoryDisplay.Init(_playerInventory, OnItemSelectHighlightBodyParts, EquipItem, UnequipItem);
        }
        
        private void CreateStatsDisplays()
        {
            StatDisplay sd;
            foreach (CharacterStat cStat in _character.characterStats)
            {
                sd = Instantiate(_statDisplayPrefab, _characterStatContainer);
                sd.Init(cStat);
            }
        }

        private void InitBodyPartDisplays()
        {
            _bodyPartDisplays = new List<BodyPartDisplay>(transform.GetComponentsInChildren<BodyPartDisplay>());
            _bodyPartStatsDisplays = new List<BodyPartStatsDisplay>();
            BodyPartStatsDisplay bpStatsDisplay;

            foreach (BodyPart bodyPart in _character.bodyParts)
            {
                foreach (BodyPartDisplay bpDsiplay in _bodyPartDisplays)
                {
                    if(bpDsiplay.BodyPartType.GetType() == bodyPart.GetType())
                    {
                        if(bpDsiplay.BodyPartType.displayName == bodyPart.displayName)
                        {
                            if (bodyPart is Hand)
                            {
                                bpStatsDisplay = Instantiate(_handStatsDisplayPrefab, _bodyPartStatDisplayContainer);
                                ((HandStatsDisplay)bpStatsDisplay).WeaponSlot.Init(null, _inventoryDisplay.OnInventoryItemPointerEnter, _inventoryDisplay.OnInventoryItemPointerExit, _inventoryDisplay.DraggedItemInventoryDisplay, EquipItem, UnequipItem);
                            }
                            else
                            {
                                bpStatsDisplay = Instantiate(_bodyPartStatsDisplayPrefab, _bodyPartStatDisplayContainer);
                            }
                            bpStatsDisplay.Init(bodyPart, bpDsiplay._iBodyPartGraphic, _character.Player.SelectBodyPartCallback);
                            bpStatsDisplay.ItemSlot.Init(null, _inventoryDisplay.OnInventoryItemPointerEnter, _inventoryDisplay.OnInventoryItemPointerExit, _inventoryDisplay.DraggedItemInventoryDisplay, EquipItem, UnequipItem);
                            _bodyPartStatsDisplays.Add(bpStatsDisplay);
                        }
                    }
                }
            }
        }

        private void OnItemSelectHighlightBodyParts(Item targetItem, bool itemSelected)
        {
            BodyPart bPart = targetItem.bodyPartType;
            foreach (BodyPartStatsDisplay bpStat in _bodyPartStatsDisplays)
            {
                if(bpStat.BodyPart.GetType() == bPart.GetType())
                {
                    bpStat.SetDisplayHighlight(itemSelected);
                }
            }
        }

        public Item EquipItem(Item i)
        {
            return _character.EquipItem(i, null);
        }

        public Item UnequipItem(Item i)
        {
            _character.UnequipItem(i);
            _inventoryDisplay.CreateItemInventoryDisplay(i);
            return i;
        }

        private void AddHealthPointsChangeListeners()
        {
            foreach (BodyPart bodyPart in _character.bodyParts)
            {
                HealthPoints hpStat = bodyPart.GetStat<HealthPoints>() as HealthPoints;
                hpStat.OnStatChangeEvent += OnHealthPointsChange;
            }
        }

        private void OnHealthPointsChange(string statString, int changeDifference)
        {
            SetPlayerNameAndTotalHealthPoints();
        }

        private void SetPlayerNameAndTotalHealthPoints()
        {
            _tPlayerName.text = _character.PlayerName + " (" + _character.TotalHealthPointsPercent.ToString() + "%)";

        }
    }
}