﻿using UnityEngine;
using System.Collections;

namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName ="BlockChance", menuName = "EW_Stats/BlockChance")]
    public class BlockChance : CharacterStat
    {
        public bool CanBlock { get; private set; }
        public void OnDefendTryBlock(object o, object p)
        {
            CanBlock = false;

            if (Random.Range(0, 101) < Value)//temporary random
                CanBlock = true;
        }

        public override string ToString()
        {
            return base.ToString() + "%";
        }
    }
}