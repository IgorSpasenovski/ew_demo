﻿using UnityEngine;
using System.Collections;
using TMPro;

public class AnimatedStatTextChange : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _text;

    public void Init(TextMeshProUGUI statDisplayText, int changeDifference)
    {
        RectTransform rtrans = transform as RectTransform;
        rtrans.SetParent(statDisplayText.transform.parent);
        rtrans.anchorMin = statDisplayText.rectTransform.anchorMin;
        rtrans.anchorMax = statDisplayText.rectTransform.anchorMax;
        rtrans.pivot = statDisplayText.rectTransform.pivot;
        rtrans.sizeDelta = statDisplayText.rectTransform.sizeDelta;
        rtrans.offsetMax = statDisplayText.rectTransform.offsetMax;
        rtrans.offsetMin = statDisplayText.rectTransform.offsetMin;
        rtrans.anchoredPosition = statDisplayText.rectTransform.anchoredPosition;

        bool positiveDiff = changeDifference > 0;
        string transparentColor = "#" + ColorUtility.ToHtmlStringRGBA(new Color(0f, 0f, 0f, 0f));
        _text.text = string.Format("<color={2}>{3}</color>  {0}{1}", positiveDiff ? "+" : "", changeDifference.ToString(), transparentColor, statDisplayText.text);
        _text.alignment = statDisplayText.alignment;
        _text.fontSize = statDisplayText.fontSize;
        _text.color = positiveDiff ? Color.green : Color.red;

        StartCoroutine(FadeText());
    }

    private IEnumerator FadeText()
    {
        float alphaStep = 0.002f;
        Color targetColor = _text.color;

        yield return new WaitForSeconds(0.3f);

        while(targetColor.a > 0f)
        {
            targetColor.a -= alphaStep;
            _text.color = targetColor;
            yield return null;
        }

        Destroy(gameObject);
    }
}
