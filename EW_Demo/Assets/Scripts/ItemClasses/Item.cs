﻿using UnityEngine;
using System.Collections;
using EW.CharacterClasses.BodyParts;

namespace EW.ItemClasses
{
    [CreateAssetMenu(fileName = "Item", menuName = "EW_Items/Item")]
    public class Item : ScriptableObject
    {
        public string itemName;
        public Sprite _displaySprite;
        public ItemRarity _itemRarity;
        //rarity
        //material
        //quality
        public BodyPart bodyPartType;
        public ItemStat[] itemStats;

        [System.NonSerialized()]
        private bool _isEquipped;
        public bool IsEquipped {get=>_isEquipped;set=>_isEquipped=value;}
        public void OnValidate()
        {
            foreach (ItemStat stat in itemStats)
            {
                stat.GenerateDescription();
            }
        }

        public void ResetItemStats()
        {
            foreach (ItemStat iStat in itemStats)
            {
                iStat.IsApplied = false;
            }
        }
    }
}