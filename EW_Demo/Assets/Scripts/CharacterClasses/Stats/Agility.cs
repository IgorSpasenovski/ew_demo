﻿using EW.CharacterClasses.BodyParts;
using EW.DuelCombatClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName = "Agility", menuName = "EW_Stats/Agility")]
    public class Agility : CharacterStat
    {
        
        public void OnRolledPair(object character, object p)
        {
            if (Value > 0)
            {
                if (character is Character myCharacter)
                {
                    //HEAL MOST DAMAGED BODY PART FOR AGILITY VALUE
                    BodyPart mostDamagedBodyPart = null;
                    foreach (BodyPart bodyPart in myCharacter.bodyParts)
                    {
                        if (mostDamagedBodyPart == null)
                            mostDamagedBodyPart = bodyPart;

                        if (bodyPart.MissingHealthPoints > mostDamagedBodyPart.MissingHealthPoints)
                            mostDamagedBodyPart = bodyPart;
                    }

                    mostDamagedBodyPart.HealthPoints += Value;
                    CombatLog.Instance.AddLog(myCharacter.PlayerName + " rolls pair and heals " + mostDamagedBodyPart.displayName + " for " + Value, myCharacter.PlayerName);
                }
            }
        }
    }
}