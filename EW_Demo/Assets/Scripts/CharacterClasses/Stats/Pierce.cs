﻿using UnityEngine;
using System.Collections;
using EW.ItemClasses;

namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName = "Pierce", menuName = "EW_Stats/Pierce")]

    public class Pierce : CharacterStat
    {
        private int _pierceWeaponsEquipped;
        public bool PierceCapable { get => _pierceWeaponsEquipped > 0; }
        public bool CanPierce { get; private set; }

        public override void ApplyItemStat(ItemStat iStat)
        {
            _pierceWeaponsEquipped++;

            base.ApplyItemStat(iStat);

        }

        public override void RemoveItemStat(ItemStat iStat)
        {
            _pierceWeaponsEquipped--;

            base.RemoveItemStat(iStat);
        }

        public void OnAttackTryPierce(object o, object p)
        {
            CanPierce = false;
            if(PierceCapable)
            {
                if (Random.Range(0, 101) < Value)//temporary random
                    CanPierce = true;
            }
        }

        public override string ToString()
        {
            if (PierceCapable)
                return base.ToString() + "%";
            else
                return _displayName + " : 0%";
        }
    }
}
