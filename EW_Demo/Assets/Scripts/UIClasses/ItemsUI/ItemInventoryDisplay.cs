﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using EW.ItemClasses;
using UnityEngine.EventSystems;

namespace EW.UIClasses.ItemsUI
{
    public delegate Item OnEquippedItem(Item i);

    public class ItemInventoryDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler
    {
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private Image _displayImage;
        [SerializeField] private Image _rarityImage;
        [Space]
        [SerializeField] private ColorBlock _btnColors;
        private Color _transparentColor;
        private Item _item;
        public Item CurrentItem { get => _item; }

        public delegate void OnInventoryItemPointerAction(Item i);
        private OnInventoryItemPointerAction OnInventoryItemPointerEnter;
        private OnInventoryItemPointerAction OnInventoryItemPointerExit;

        private ItemInventoryDisplay _draggedItemInventoryDisplay;

        
        private OnEquippedItem EquipItemCallback;
        private OnEquippedItem UnequipItemCallback;
        public void Init(Item i, OnInventoryItemPointerAction pointerEnterCallback, OnInventoryItemPointerAction pointerExitCallback, ItemInventoryDisplay draggedItem, OnEquippedItem equipItemCallback, OnEquippedItem unequipItemCallback)
        {
            SetDisplayItem(i);

            OnInventoryItemPointerEnter = pointerEnterCallback;
            OnInventoryItemPointerExit = pointerExitCallback;
            EquipItemCallback = equipItemCallback;
            UnequipItemCallback = unequipItemCallback;

            _draggedItemInventoryDisplay = draggedItem;
            _transparentColor = new Color(0f, 0f, 0f, 0f);

        }

        public void SetDisplayItem(Item i)
        {
            _item = i;
            if (_item != null)
            {
                _displayImage.preserveAspect = true;
                _displayImage.sprite = i._displaySprite;
                _rarityImage.color = i._itemRarity._color;
            }
            else
            {
                _displayImage.sprite = null;
                _rarityImage.color = _transparentColor;
            }
        }

        public void SetAsDraggedItem(ItemInventoryDisplay invDisplay)
        {
            SetDisplayItem(invDisplay._item);
            transform.position = invDisplay.transform.position;
            _backgroundImage.raycastTarget = false;
            _backgroundImage.color = _btnColors.highlightedColor;
            gameObject.SetActive(true);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _backgroundImage.color = _btnColors.highlightedColor;

            OnInventoryItemPointerEnter?.Invoke(_item);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_isDragged)
                return;

            _backgroundImage.color = _btnColors.normalColor;

            OnInventoryItemPointerExit?.Invoke(_item);
        }

        private Vector2 lastMousePosition;
        private Transform _lastParent;
        private bool _isDragged = false;
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (_item == null)
                return;

            lastMousePosition = eventData.position;
            _isDragged = true;
            _backgroundImage.raycastTarget = false;
            //_lastParent = transform.parent;
            //transform.SetParent(transform.parent.parent.parent.parent);
            _backgroundImage.color = _btnColors.disabledColor;
            _displayImage.color = _btnColors.disabledColor;

            _draggedItemInventoryDisplay.SetAsDraggedItem(this);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_item == null)
                return;

            Vector2 currentMousePosition = eventData.position;
            Vector2 diff = currentMousePosition - lastMousePosition;
            RectTransform rect = _draggedItemInventoryDisplay.GetComponent<RectTransform>();

            Vector3 newPosition = rect.position + new Vector3(diff.x, diff.y, transform.position.z);
            Vector3 oldPos = rect.position;
            rect.position = newPosition;
            if (!IsRectTransformInsideSreen(rect))
            {
                rect.position = oldPos;
            }
            lastMousePosition = currentMousePosition;
        }

        private bool IsRectTransformInsideSreen(RectTransform rectTransform)
        {
            bool isInside = false;
            Vector3[] corners = new Vector3[4];
            rectTransform.GetWorldCorners(corners);
            int visibleCorners = 0;
            Rect rect = new Rect(0, 0, Screen.width, Screen.height);
            foreach (Vector3 corner in corners)
            {
                if (rect.Contains(corner))
                {
                    visibleCorners++;
                }
            }
            if (visibleCorners == 4)
            {
                isInside = true;
            }
            return isInside;
        }

        public void OnDrop(PointerEventData eventData)
        {
            RectTransform invPanel = transform as RectTransform;
            print("Drop item");
            if (!RectTransformUtility.RectangleContainsScreenPoint(invPanel, Input.mousePosition))
            {
                //drop item
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (_item == null)
                return;

            _isDragged = false;
            print("end drag");
            //transform.SetParent(_lastParent);
            _backgroundImage.raycastTarget = true;

            _backgroundImage.color = _btnColors.normalColor;
            _displayImage.color = Color.white;
            OnInventoryItemPointerExit?.Invoke(_item);

            _draggedItemInventoryDisplay.gameObject.SetActive(false);

        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if(eventData.button == PointerEventData.InputButton.Right)
            {
                if (_item != null)
                {
                    if (_item.IsEquipped)
                        UnequipItem();
                    else
                        EquipItem();
                }
            }
        }

        public void EquipItem()
        {
            if (_isDragged)
                OnEndDrag(null);

            OnInventoryItemPointerExit?.Invoke(_item);

            _item = EquipItemCallback?.Invoke(_item);

            if (_item == null)
            {
                Destroy(gameObject);
            }
            else
            {
                SetDisplayItem(_item);
            }
        }

        public void UnequipItem()
        {
            if (_isDragged)
                OnEndDrag(null);

            OnInventoryItemPointerExit?.Invoke(_item);

            UnequipItemCallback?.Invoke(_item);
        }

        private void OnDestroy()
        {
            Destroy(_item);// TODO: remove when optimized not to instantiate SOs
        }
    }
}
