﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using EW.PlayerClasses;

namespace EW.DuelCombatClasses
{
    public class CombatLog : MonoBehaviour
    {

        private static CombatLog _instance;
        public static CombatLog Instance { get => _instance; }

        public bool _enabled = true;
        public bool _logToConsole = false;
        public Text _tCombatLog;

        private int _logsCount = 0;

        private Dictionary<string, string> _loggerColors;

        private void Awake()
        {
            _instance = this;
            SetEnabled(_enabled);
            _logsCount = 0;
        }

        public void SetEnabled(bool enabled)
        {
            _enabled = enabled;
            gameObject.SetActive(_enabled);
        }

        private string _lineColor;
        public void AddLog(string log, string loggerName = "")
        {
            _logsCount += 1;

            _lineColor = "white";

            if (loggerName != "")
                _lineColor = GetLineColor(loggerName);

            if(_tCombatLog != null)
                _tCombatLog.text += string.Format("<color={2}>{0}. {1} </color>\n", _logsCount, log, _lineColor);

            if(_logToConsole)
            {
                Debug.Log(string.Format("{0}. {1}", _logsCount, log));
            }
        }

        public void ClearLog()
        {
            _logsCount = 0;
            _tCombatLog.text = "";
        }

        public void RegisterLoggerLineColor(string loggerName, string hexColor)
        {
            if (_loggerColors == null)
                _loggerColors = new Dictionary<string, string>();

            _loggerColors.Add(loggerName, hexColor);
        }

        private string GetLineColor(string callerName)
        {
            string lineColor = "white";

            if(_loggerColors != null)
            {
                if(_loggerColors.ContainsKey(callerName))
                {
                    lineColor = _loggerColors[callerName];
                }
            }

            return lineColor;
        }

    }
}
