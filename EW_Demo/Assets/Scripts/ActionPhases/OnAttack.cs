﻿using UnityEngine;

[CreateAssetMenu(fileName = "OnAttack", menuName = "EW_ActionPhases/OnAttack")]
public class OnAttack : ActionPhase
{
    public void AttackActivateTest()
    {
        Debug.Log("attack activate test");
    }

    public void CustomActionEvent(object o, object p)
    {
        int gInt = default;
        string gString = default;
        if (o is int myInt)
        {
            Debug.Log(myInt);
            gInt = myInt;
        }
        Debug.Log(gInt);
        if (p is string myString)
        {
            gString = myString;
        }
        Debug.Log(gString);


    }
}
