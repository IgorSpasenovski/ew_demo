﻿using UnityEngine;
using System.Collections;
using EW.CharacterClasses;
using EW.PlayerClasses;
using EW.CharacterClasses.BodyParts;
using UnityEngine.Events;

namespace EW.DuelCombatClasses
{
    public class DuelCombatController
    {
        private const int duelEndHealthPointsThreshold = 25;
        private Player player1;
        private Player player2;
        private Player ActivePlayer
        {
            get
            {
                if (player1.IsActivePlayer)
                    return player1;
                else
                    return player2;
            }
        }
        private Player TargetPlayer
        {
            get
            {
                if (!player1.IsActivePlayer)
                    return player1;
                else
                    return player2;
            }
        }
        private PlayerTurnPhase pendingPlayerPhase;
        private PlayerTurnPhase PendingPlayerPhase
        {
            get { return pendingPlayerPhase; }
            set
            {
                pendingPlayerPhase = value;

                SetPhaseBreak();

                if (ActivePlayer != null)
                {
                    ActivePlayer.PendingPhase = value;
                }
            }
        }

        private float phaseBreakStartTime = 0.0f;
        private float phaseBreakDuration = 0.0f;

        public UnityAction<string> EndDuel;

        public DuelCombatController()
        {

        }

        public void PrepareDuel(Player p1, Player p2)
        {
            player1 = p1;
            player1.SelectBodyPartCallback = SelectBodyPartTarget;
            player2 = p2;
            player2.SelectBodyPartCallback = SelectBodyPartTarget;

            //determine who goes first from agility

            player1.IsActivePlayer = true;//TEMPORARY

            BeginDuel();
        }

        
        private void BeginDuel()
        {
            PendingPlayerPhase = PlayerTurnPhase.TurnStarted;
        }

        public void CheckTurnPhaseExecution()
        {
            
            if(AutoExecutePhase(PlayerTurnPhase.TurnStarted))
            {
                //phase OnTurn, OnOpponentTurn - no action from player
                CombatLog.Instance.AddLog(ActivePlayer.playerName + "'s turn started. =========", ActivePlayer.playerName);
                PendingPlayerPhase = PlayerTurnPhase.PlayerActions;
            }

            if (CanExecutePhase(PlayerTurnPhase.PlayerActions))
            {
                // action from active player select target, use consumables
                PendingPlayerPhase = PlayerTurnPhase.RollDice;
            }

            if (CanExecutePhase(PlayerTurnPhase.RollDice))
            {
                //phase OnDiceRoll, OnOpponentDiceRoll - action from active player roll dice
                PendingPlayerPhase = PlayerTurnPhase.ProcessAttack;
            }

            if(AutoExecutePhase(PlayerTurnPhase.ProcessAttack))
            {
                //calculate attack
                ActivePlayer.ProcessAttack();

                PendingPlayerPhase = PlayerTurnPhase.ProcessDefense;

            }

            if (AutoExecutePhase(PlayerTurnPhase.ProcessDefense))
            {
                TargetPlayer.ProcessDefense(ActivePlayer.TargetBodyPart, ActivePlayer.PlayerCharacter.CurrentAttack);

                PendingPlayerPhase = PlayerTurnPhase.EndTurn;
            }


            if (AutoExecutePhase(PlayerTurnPhase.EndTurn))
            {
                CombatLog.Instance.AddLog(ActivePlayer.playerName + "'s turn ended.", ActivePlayer.playerName);

                player1.IsActivePlayer = !player1.IsActivePlayer;
                player2.IsActivePlayer = !player2.IsActivePlayer;

                PendingPlayerPhase = PlayerTurnPhase.TurnStarted;

                CheckDuelEnd();
            }
            

        }

        private void CheckDuelEnd()
        {
            if(player1.PlayerCharacter.TotalHealthPointsPercent < duelEndHealthPointsThreshold)
            {
                EndDuel(player2.playerName);
            }
            if (player2.PlayerCharacter.TotalHealthPointsPercent < duelEndHealthPointsThreshold)
            {
                EndDuel(player1.playerName);

            }
        }

        private bool CanExecutePhase(PlayerTurnPhase phaseToMatch)
        {
            bool executePhase = ((ActivePlayer.PlayerPhase == phaseToMatch) && (pendingPlayerPhase == phaseToMatch));

            executePhase = executePhase && PhaseBreakExpired();

            return executePhase;
        }

        private bool AutoExecutePhase(PlayerTurnPhase phaseToMatch)
        {
            bool executePhase = (pendingPlayerPhase == phaseToMatch);

            executePhase = executePhase && PhaseBreakExpired();

            return executePhase;
        }

        private bool PhaseBreakExpired()
        {
            bool breakExpired = true;

            if (phaseBreakDuration > 0.0f)
            {
                if (Time.time < phaseBreakStartTime + phaseBreakDuration)
                {
                    breakExpired = false;
                }
                else
                {
                    phaseBreakDuration = 0.0f;
                    phaseBreakStartTime = 0.0f;
                }
            }

            return breakExpired;
        }

        private void SetPhaseBreak()
        {
            float duration = GetPhaseBreakDuration(PendingPlayerPhase);

            phaseBreakStartTime = Time.time;
            phaseBreakDuration = duration;
        }

        private float GetPhaseBreakDuration(PlayerTurnPhase turnPhase)
        {
            float phaseBreakDuration = 0.0f;
            switch (turnPhase)
            {
                case PlayerTurnPhase.TurnStarted:
                    phaseBreakDuration = 2.0f;
                    break;
                case PlayerTurnPhase.PlayerActions:
                    phaseBreakDuration = 0.0f;
                    break;
                case PlayerTurnPhase.RollDice:
                    phaseBreakDuration = 1.0f;
                    break;
                case PlayerTurnPhase.ProcessAttack:
                    phaseBreakDuration = 4.0f;
                    break;
                case PlayerTurnPhase.ProcessDefense:
                    phaseBreakDuration = 1.0f;
                    break;
                case PlayerTurnPhase.EndTurn:
                    phaseBreakDuration = 1.0f;
                    break;
                default:
                    phaseBreakDuration = 0.0f;
                    break;
            }
            return phaseBreakDuration;
        }

        public void SelectBodyPartTarget(BodyPart target)
        {
            if(TargetPlayer.PlayerCharacter.bodyParts.Contains(target))
                ActivePlayer.SelectBodyPartTarget(target);
        }

        public void RollDice()
        {
            ActivePlayer.RollDice();
        }
    }

    public enum PlayerTurnPhase
    {
        TurnStarted,
        PlayerActions,
        RollDice,
        ProcessAttack,
        ProcessDefense,
        EndTurn
    }

}