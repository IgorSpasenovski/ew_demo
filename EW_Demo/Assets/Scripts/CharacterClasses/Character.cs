﻿using System.Collections.Generic;
using UnityEngine;
using EW.CharacterClasses.Stats;
using EW.CharacterClasses.BodyParts;
using EW.ItemClasses;
using EW.DuelCombatClasses;
using EW.PlayerClasses;

namespace EW.CharacterClasses
{
    
    [CreateAssetMenu(fileName = "Character", menuName = "EW_Character")]
    public class Character : ScriptableObject
    {
        //name
        //body parts list
        //character base stat list (primary secondary?)
        //character action stat list
        public CharacterStat[] baseStatsTemplate;
        public BodyPart[] baseBodyPartAssets;

        [HideInInspector]
        public List<CharacterStat> characterStats;
        [HideInInspector]
        public List<BodyPart> bodyParts;

        public Attack CurrentAttack { get; private set; }
        public string PlayerName { get; set; }
        public Player Player { get; set; }
        
        public int TotalHealthPointsPercent
        {
            get
            {
                float totalHp = 0;
                float currentHp = 0;

                foreach (BodyPart bodyPart in bodyParts)
                {
                    totalHp += bodyPart.TotalHealthPoints;
                    currentHp += bodyPart.HealthPoints;
                }

                int hpPercent = Mathf.RoundToInt(currentHp / totalHp * 100f);

                return hpPercent;
            }
        }

        public void InitCharacter()
        {
            characterStats = new List<CharacterStat>();
            foreach (CharacterStat cStat in baseStatsTemplate)
            {
                CharacterStat cStatClone = Instantiate(cStat);
                cStatClone.name = cStat.name;
                characterStats.Add(cStatClone);
            }

            bodyParts = new List<BodyPart>();
            foreach (BodyPart bodyPartAsset in baseBodyPartAssets)
            {
                BodyPart bp = Instantiate(bodyPartAsset);
                bp.InitializeBodyPart(ApplyEquippedItemStats, RemoveUnequippedItemStats, GetStat<Weaken>);
                bodyParts.Add(bp);
            }

            CurrentAttack = new Attack();
        }

        public void ModifyStat()
        {
            foreach (CharacterStat cStat in characterStats)
            {
                if (cStat.name == "TestStat")
                {
                    cStat.Value += 1;
                    Debug.Log(cStat.name + " " + cStat.Value.ToString());
                }
            }

        }

        public void CalculateTotalAttack(DiceRoll diceRoll)
        {
            CombatLog.Instance.AddLog(PlayerName + " rolls " + diceRoll.ToString(), PlayerName);
            if (diceRoll.IsPair)
            {
                ActivatePhase<OnRolledPair>();
            }
            //OnAttack phase
            ActivatePhase<OnAttack>();

            CurrentAttack.SetAttack(diceRoll.CurrentDiceRollSum, GetStat<AttackPower>().Value, ((Pierce)(GetStat<Pierce>())).CanPierce);

            CombatLog.Instance.AddLog(PlayerName + " total attack is " + CurrentAttack.ToString(), PlayerName);

        }

        public void ProcessDefense(BodyPart attackedBodyPart, Attack incomingAttack)
        {
            ActivatePhase<OnDefend>();

            attackedBodyPart.OnDeselectedAttackTarget?.Invoke();

            //dodge
            if(((Dodge)GetStat<Dodge>()).CanDodge)
            {
                CombatLog.Instance.AddLog(PlayerName + " dodged " + incomingAttack.ToString() + " attack damage directed at " + attackedBodyPart.displayName, PlayerName);
                return;
            }

            //block
            if(((BlockChance)GetStat<BlockChance>()).CanBlock)
            {
                CombatLog.Instance.AddLog(PlayerName + " blocked " + incomingAttack.ToString() + " attack damage directed at " + attackedBodyPart.displayName, PlayerName);
                return;
            }

            int processedAttack = incomingAttack.Damage;
            if(incomingAttack.PiercingAttack == false)
            {
                processedAttack -= attackedBodyPart.GetStat<BodyPartDefense>().Value;
                processedAttack -= GetStat<Defense>().Value;
            }

            processedAttack = Mathf.Clamp(processedAttack, 0, incomingAttack.Damage);

            Weaken weakenStat = GetStat<Weaken>() as Weaken;
            if(weakenStat != null)
            {
                processedAttack = weakenStat.AmplifyDamage(processedAttack);
            }

            attackedBodyPart.OnBodyPartAttacked(processedAttack);
            CombatLog.Instance.AddLog(PlayerName + "'s " + attackedBodyPart.displayName + " gets damaged for " + processedAttack.ToString(), PlayerName);

        }

        public void ActivatePhase<T>() where T : ActionPhase
        {
            foreach (CharacterStat cStat in characterStats)
            {
                foreach (ActionActivationPhase statActionPhase in cStat.statActionPhases)
                {
                    if (statActionPhase.actionPhase.GetType() == typeof(T))
                    {
                        if (statActionPhase.RunActivationChance())
                        {
                            statActionPhase.OnActionPhaseEvent.Invoke(this, null);
                        }
                    }
                }
            }       
        }

        private Item _previousItem = null;
        //public Item EquipItem(Item itemToEquip)
        //{
        //    _previousItem = null;
        //    foreach (BodyPart bp in bodyParts)
        //    {
        //        if (bp.GetType() == itemToEquip.bodyPartType.GetType())
        //        {
        //            if (itemToEquip.GetType() == typeof(WeaponItem))
        //            {
        //                Hand hand = (Hand)bp;
        //                if(hand.CanEquipWeapon((WeaponItem)itemToEquip))
        //                {
        //                    _previousItem = hand.EquipWeapon((WeaponItem)itemToEquip);
        //                    CombatLog.Instance.AddLog(PlayerName + " equips " + itemToEquip.itemName + " on " + hand._displayWeaponSlotName, PlayerName);
        //                    if (((WeaponItem)itemToEquip)._weaponHandType.HasFlag(WeaponHandType.TwoHand) == false)
        //                    {
        //                        break;
        //                    }
        //                }
                        
        //            }
        //            else
        //            {
        //                _previousItem = bp.EquipItem(itemToEquip);
        //                CombatLog.Instance.AddLog(PlayerName + " equips " + itemToEquip.itemName + " on " + bp.displayName, PlayerName);
        //            }
        //        }
        //    }

        //    return _previousItem;
        //}
        public Item EquipItem(Item itemToEquip, BodyPart targetBodyPart = null)
        {
            if (itemToEquip is WeaponItem)
            {
                _previousItem = EquipWeapon(itemToEquip as WeaponItem, targetBodyPart);
            }
            else
            {
                _previousItem = EquipArmor(itemToEquip);
            }

            return _previousItem;
        }

        private Item EquipArmor(Item itemToEquip)
        {
            _previousItem = null;
            foreach (BodyPart bodyPart in bodyParts)
            {
                if (bodyPart.GetType() == itemToEquip.bodyPartType.GetType())
                {
                    _previousItem = bodyPart.EquipItem(itemToEquip);

                    CombatLog.Instance.AddLog(PlayerName + " equips " + itemToEquip.itemName + " on " + bodyPart.displayName + (_previousItem != null ? " unequipped " + _previousItem.itemName : ""), PlayerName);

                }
            }
                    

            return _previousItem;
        }
        private Item EquipWeapon(WeaponItem weaponToEquip, BodyPart targetBodyPart = null)
        {
            _previousItem = null;

            if((targetBodyPart != null)&& weaponToEquip._weaponHandType.HasFlag(WeaponHandType.TwoHand) == false)
            {
                Hand hand = (Hand)targetBodyPart;
                if (hand.CanEquipWeapon(weaponToEquip))
                {
                    _previousItem = hand.EquipWeapon(weaponToEquip);
                    CombatLog.Instance.AddLog(PlayerName + " equips " + weaponToEquip.itemName + " on " + hand._displayWeaponSlotName + (_previousItem != null ? " unequipped " + _previousItem.itemName : ""), PlayerName);
                }
            }
            else
            {
                foreach (BodyPart bodyPart in bodyParts)
                {
                    if (bodyPart.GetType() == weaponToEquip.bodyPartType.GetType())
                    {
                        Hand hand = (Hand)bodyPart;
                        if (hand != null)
                        {
                            if (hand.CanEquipWeapon(weaponToEquip))
                            {
                                _previousItem = hand.EquipWeapon(weaponToEquip);
                                CombatLog.Instance.AddLog(PlayerName + " equips " + weaponToEquip.itemName + " on " + hand._displayWeaponSlotName + (_previousItem!=null?" unequipped " + _previousItem.itemName:""), PlayerName);
                                if (weaponToEquip._weaponHandType.HasFlag(WeaponHandType.TwoHand) == false)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            return _previousItem;

        }
        //called from BodyPart when equipping an item
        public void ApplyEquippedItemStats(ItemStat iStat)
        {
            CharacterStat cStat = GetStat(iStat.characterStatType);
            if (cStat != null)
            {
                cStat.ApplyItemStat(iStat);
            }
            
        }

        public void UnequipItem(Item itemToUnequip)
        {
            //foreach (BodyPart bp in bodyParts)
            //{
            //    if (bp.GetType() == itemToUnequip.bodyPartType.GetType())
            //    {
            //        bp.UnequpItem();
            //    }
            //}

            if (itemToUnequip is WeaponItem)
            {
                UnequipWeapon(itemToUnequip as WeaponItem);
            }
            else
            {
                UnequipArmor(itemToUnequip);
            }
        }
        private void UnequipArmor(Item itemToUnequip)
        {
            foreach (BodyPart bp in bodyParts)
            {
                if (bp.GetType() == itemToUnequip.bodyPartType.GetType())
                {
                    if(bp.EquippedItem != null)
                    {
                        if(bp.EquippedItem == itemToUnequip)
                        {
                            bp.UnequpItem();
                            CombatLog.Instance.AddLog(PlayerName + " unequips " + itemToUnequip.itemName + " from " + bp.displayName, PlayerName);
                        }
                    }
                    
                }
            }
        }
        private void UnequipWeapon(WeaponItem weaponToUnequip)
        {
            foreach (BodyPart bp in bodyParts)
            {
                if (bp.GetType() == weaponToUnequip.bodyPartType.GetType())
                {
                    Hand hand = (Hand)bp;
                    if (hand != null)
                    {
                        if (hand.EquippedWeapon != null)
                        {
                            if (hand.EquippedWeapon == weaponToUnequip)
                            {
                                hand.UnequpWeapon();
                                CombatLog.Instance.AddLog(PlayerName + " unequips " + weaponToUnequip.itemName + " from " + hand._displayWeaponSlotName, PlayerName);

                            }
                        }
                    }
                }
            }
        }

        public void UnequipFromBodyPart(BodyPart bp)
        {
            bp.UnequpItem();
        }

        public void RemoveUnequippedItemStats(ItemStat iStat)
        {
            CharacterStat cStat = GetStat(iStat.characterStatType);
            if (cStat != null)
            {
                cStat.RemoveItemStat(iStat);
            }
        }

        public CharacterStat GetStat(CharacterStat iStat)
        {
            foreach (CharacterStat cStat in characterStats)
            {
                if(cStat.GetType() == iStat.GetType())
                {
                    return cStat;
                }
            }
            return null;
        }

        public CharacterStat GetStat<T>() where T : CharacterStat
        {
            foreach (CharacterStat cStat in characterStats)
            {
                if (cStat.GetType() == typeof(T))
                {
                    return cStat;
                }
            }
            return null;
        }

        public void DestroyCharacter()
        {
            for (int i = characterStats.Count - 1; i >= 0; i--)
            {
                Destroy(characterStats[i]);
            }

            for (int i = bodyParts.Count - 1; i >= 0; i--)
            {
                Destroy(bodyParts[i]);
            }
        }

    }
}