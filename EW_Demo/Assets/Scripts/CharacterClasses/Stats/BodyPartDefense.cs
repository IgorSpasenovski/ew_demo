﻿using UnityEngine;
using System.Collections;

namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName = "BodyPartDefense", menuName = "EW_Stats/BodyPartDefense")]
    public class BodyPartDefense : CharacterStat
    {
        public override string ToString()
        {
            return "D: " + Value.ToString();
        }
    }
}
