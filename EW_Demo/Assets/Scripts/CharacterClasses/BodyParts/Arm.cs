﻿using UnityEngine;
using System.Collections;

namespace EW.CharacterClasses.BodyParts
{
    [CreateAssetMenu(fileName = "Arm", menuName = "EW_BodyParts/Arm")]
    public class Arm : BodyPart
    {

        public override void OnBodyPartDestroyed()
        {
            base.OnBodyPartDestroyed();
            Debug.Log("Destroyed Arm");
        }
    }
}
