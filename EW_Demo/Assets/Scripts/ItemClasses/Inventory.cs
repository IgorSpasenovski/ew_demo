﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace EW.ItemClasses
{
    [CreateAssetMenu(fileName = "NewInventory", menuName = "EW_Items/Inventory")]
    public class Inventory : ScriptableObject
    {
        public List<Item> _items;
    }
}