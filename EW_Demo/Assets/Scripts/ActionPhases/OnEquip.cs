﻿using UnityEngine;

[CreateAssetMenu(fileName = "OnEquip", menuName = "EW_ActionPhases/OnEquip")]
public class OnEquip : ActionPhase
{
    public void OnEquipCallback(object o, object p)
    {
        Debug.Log("equip item ");
    }
}
