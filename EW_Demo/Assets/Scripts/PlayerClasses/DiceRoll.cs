﻿using System.Collections.Generic;
using UnityEngine;

namespace EW.PlayerClasses
{
    public class DiceRoll
    {

        public List<int> _currentDiceRoll;
        private int _currentDiceRollSum;
        public int CurrentDiceRollSum
        {
            get => _currentDiceRollSum;
        }
        public bool IsPair
        {
            get
            {
                if (_currentDiceRoll != null)
                {
                    if (_currentDiceRoll.Count == 2)
                    {
                        if (_currentDiceRoll[0] == _currentDiceRoll[1])
                            return true;
                    }
                }

                return false;
            }
        }
        public int CurrentNumberOfDice { get => _currentDiceRoll.Count; }

        private int _numberOfDice = 2;

        public int _minDiceRoll = 1;
        public int _maxDiceRoll = 6;
        private int _perValueDicePoolSize = 30;
        private List<int> _dicePool;

        public DiceRoll()
        {
            _currentDiceRoll = new List<int>();
            FillDicePool();
        }

        public void RollDice(int extraDice = 0, bool guaranteedPair = false)
        {
            _currentDiceRoll.Clear();
            _currentDiceRollSum = 0;

            for (int i = 0; i < _numberOfDice + extraDice; i++)
            {
                if (guaranteedPair && i > 0)
                    _currentDiceRoll.Add(_currentDiceRoll[0]);
                else
                _currentDiceRoll.Add(GetRollFromDicePool());

                _currentDiceRollSum += _currentDiceRoll[i];
            }
        }

        private int GetRollFromDicePool()
        {
            if (_dicePool.Count < 20)
                FillDicePool();

            int rndIndex = Random.Range(0, _dicePool.Count);

            int roll = _dicePool[rndIndex];
            _dicePool.RemoveAt(rndIndex);

            return roll;
        }

        private void FillDicePool()
        {
            if (_dicePool == null)
                _dicePool = new List<int>();
            else
                _dicePool.Clear();

            for(int i = _minDiceRoll; i<=_maxDiceRoll; i++)
            {
                for (int j = 0; j < _perValueDicePoolSize; j++)
                {
                    _dicePool.Add(i);
                }
            }
        }

        public override string ToString()
        {
            string msg = "";
            if(IsPair)
            {
                msg += string.Format("a pair of {0}s", _currentDiceRoll[0].ToString());
            }
            else
            {
                for (int i = 0; i < _currentDiceRoll.Count; i++)
                {
                    msg += string.Format("{0}{1}", _currentDiceRoll[i].ToString(), i == _currentDiceRoll.Count - 2 ? " and " : ", ");
                }
            }

            msg += " total of " + CurrentDiceRollSum.ToString();

            return msg;
        }
    }
}