﻿using UnityEngine;
using System.Collections;

namespace EW.CharacterClasses.BodyParts
{
    [CreateAssetMenu(fileName = "Chest", menuName = "EW_BodyParts/Chest")]
    public class Chest : BodyPart
    {

        public override void OnBodyPartDestroyed()
        {
            base.OnBodyPartDestroyed();
            Debug.Log("Destroyed Chest");
        }
    }
}
