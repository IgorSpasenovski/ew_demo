﻿using UnityEngine;
using System.Collections;
namespace EW.UIClasses.DuelUI
{
    public class DiceDisplay : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] _faces;

        private int _currentDiceValue = 0;
        public int CurrentDiceValue { get => _currentDiceValue; }
        public void SetDiceValue(int value)
        {
            _currentDiceValue = value;

            for (int i = 0; i < _faces.Length; i++)
            {
                _faces[i].SetActive(i + 1 == _currentDiceValue);
            }
        }

    }
}