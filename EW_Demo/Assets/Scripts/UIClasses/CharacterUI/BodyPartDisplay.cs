﻿using UnityEngine;
using System.Collections;
using TMPro;
using EW.CharacterClasses.BodyParts;
using UnityEngine.UI;
using EW.CharacterClasses.Stats;

namespace EW.UIClasses.CharacterUI
{
    public class BodyPartDisplay : MonoBehaviour
    {
        //defense stat
        //health stat
        //equipped item slot
        //body part type to display
        [SerializeField]
        private BodyPart _bodyPartType;
        public BodyPart BodyPartType { get => _bodyPartType; }
        [Space]
        [SerializeField]
        public Image _iBodyPartGraphic;
        
        
        private void OnValidate()
        {
            if(_bodyPartType)
            {
                if(_bodyPartType.BodyPartGraphic)
                {
                    _iBodyPartGraphic.sprite = _bodyPartType.BodyPartGraphic;
                    //_iBodyPartGraphic.color = new Color(1f, 0f, 0f, 0.3f);
                    _iBodyPartGraphic.SetNativeSize();
                }
            }
        }
    }
}
