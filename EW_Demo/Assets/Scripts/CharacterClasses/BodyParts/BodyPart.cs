﻿using UnityEngine;
using System.Collections;
using System;
using EW.ItemClasses;
using EW.CharacterClasses.Stats;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace EW.CharacterClasses.BodyParts
{
    public class BodyPart : ScriptableObject
    {
        public string displayName = "Body Part Name";

        [SerializeField]
        private Sprite bodyPartGraphic;//in game
        public Sprite BodyPartGraphic { get => bodyPartGraphic; }
        //body part icon?     
        public List<CharacterStat> baseStatsTemplate;
        private List<CharacterStat> bodyPartStats;

        private HealthPoints healthPointsStat;
        [SerializeField]
        private int bodyPartStartHealthPoints;
        public int TotalHealthPoints { get => healthPointsStat.Value; set => healthPointsStat.BaseValue = value; }
        public int HealthPoints { get => healthPointsStat.CurrentValue; set => healthPointsStat.CurrentValue = value; }
        public int MissingHealthPoints { get => healthPointsStat.BaseCurrentValuesDifference; }

        [Space]
        [SerializeField]
        private int _weakenApplyChanceOnDestoy = 5;

        private Item equippedItem;
        public Item EquippedItem { get => equippedItem; private set => equippedItem = value; }
        private Item PreviousItem { get; set; }

        public delegate void CharacterStatsCallback(ItemStat itemStat);
        private CharacterStatsCallback ApplyItemStatsCallback;
        private CharacterStatsCallback RemoveItemStatsCallback;

        public delegate CharacterStat GetCharacterWeakenStat<T>() where T : Weaken;
        protected GetCharacterWeakenStat<Weaken> GetCharacterWeakenStatCallback;

        public delegate void OnItemEquip(Item i);
        public OnItemEquip OnItemEquipped;
        public delegate void OnItemUneqip();
        public OnItemUneqip OnItemUnequipped;

        public delegate void OnAttackTargetSet();
        public OnAttackTargetSet OnSelectedAttackTarget;
        public OnAttackTargetSet OnDeselectedAttackTarget;

        

        public void InitializeBodyPart(CharacterStatsCallback ApplyItemStats, CharacterStatsCallback RemoveItemStats, GetCharacterWeakenStat<Weaken> GetCharStat)
        {
            ApplyItemStatsCallback = ApplyItemStats;
            RemoveItemStatsCallback = RemoveItemStats;
            GetCharacterWeakenStatCallback = GetCharStat;

            bodyPartStats = new List<CharacterStat>();
            foreach (CharacterStat cStat in baseStatsTemplate)
            {
                CharacterStat cStatClone = Instantiate(cStat);
                cStatClone.name = cStat.name;
                bodyPartStats.Add(cStatClone);
            }

            healthPointsStat = GetStat<HealthPoints>() as HealthPoints;
            TotalHealthPoints = bodyPartStartHealthPoints;
            HealthPoints = TotalHealthPoints;

        }

        public virtual void OnBodyPartAttacked(int attackDamage)
        {
            HealthPoints -= attackDamage;

            if(HealthPoints <= 0)
            {
                OnBodyPartDestroyed();
            }
            //input attacker? 
        }
        public virtual void OnBodyPartDestroyed()
        {
            Weaken weakenStat = GetCharacterWeakenStatCallback() as Weaken;
            if(weakenStat != null)
            {
                weakenStat.ApplyWeaken(_weakenApplyChanceOnDestoy);
            }  
        }

        public Item EquipItem(Item equipItem)
        {
            PreviousItem = null;
            if (EquippedItem != null)
            {
                PreviousItem = EquippedItem;
                UnequpItem();
            }
            EquippedItem = equipItem;
            EquippedItem.IsEquipped = true;

            ApplyEquippedItemStats(EquippedItem);
            //call OnEquip action phase on Character (callback?)
            OnItemEquipped?.Invoke(EquippedItem);

            return PreviousItem;
        }
        public void UnequpItem()
        {
            //call OnUnequip action phase on Character (callback?)
            if (EquippedItem != null)
            {
                RemoveEquippedItemStats(EquippedItem);
                EquippedItem.IsEquipped = false;
            }
            EquippedItem = null;
            OnItemUnequipped?.Invoke();

        }

        public void ApplyEquippedItemStats(Item equippedItem)
        {
            foreach (ItemStat iStat in equippedItem.itemStats)
            {
                CharacterStat cStat = GetStat(iStat.characterStatType);
                if (cStat != null)
                {
                    //if(iStat.statActionPhase.actionPhase != null)
                        //if(iStat.statActionPhase.actionPhase.GetType() == typeof(OnEquip))
                            cStat.ApplyItemStat(iStat);
                }

                //apply to character stats
                ApplyItemStatsCallback(iStat);
            }
        }

        public void RemoveEquippedItemStats(Item equippedItem)
        {
            foreach (ItemStat iStat in equippedItem.itemStats)
            {
                CharacterStat cStat = GetStat(iStat.characterStatType);
                if (cStat != null)
                {
                    cStat.RemoveItemStat(iStat);
                }

                //apply to character stats
                RemoveItemStatsCallback(iStat);
            }
        }


        private CharacterStat GetStat(CharacterStat iStat)
        {
            foreach (CharacterStat cStat in bodyPartStats)
            {
                if (cStat.GetType() == iStat.GetType())
                {
                    return cStat;
                }
            }
            return null;
        }

        public CharacterStat GetStat<T>() where T : CharacterStat
        {
            foreach (CharacterStat cStat in bodyPartStats)
            {
                if (cStat.GetType() == typeof(T))
                {
                    return cStat;
                }
            }
            return null;
        }

        public virtual void OnDestroy()
        {
            if (EquippedItem != null)
                EquippedItem.ResetItemStats();

            for (int i = bodyPartStats.Count - 1; i >= 0; i--)
            {
                Destroy(bodyPartStats[i]);
            }
        }
    }
}