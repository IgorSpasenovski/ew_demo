﻿using UnityEngine;
using System.Collections;
namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName = "AttackPower", menuName = "EW_Stats/AttackPower")]
    public class AttackPower : CharacterStat
    {
        public void OnAttack(object o, object p)
        {

        }
    }
}