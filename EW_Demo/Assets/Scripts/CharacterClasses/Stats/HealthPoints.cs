﻿using UnityEngine;
using System.Collections;
using EW.ItemClasses;

namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName = "HealthPoints", menuName = "EW_Stats/HealthPoints")]
    public class HealthPoints : CharacterStat
    {
        private int _currentValue;
        public int CurrentValue
        {
            get
            {
                return _currentValue;
            }
            set
            {
                int newVal = Mathf.Clamp(value, 0, Value);
                int changeDifference = newVal - _currentValue;
                _currentValue = newVal;
                if (changeDifference != 0)
                    OnStatChangeEvent?.Invoke(ToString(), changeDifference);
            }
        }
        public int BaseValue
        {
            get
            {
                return _baseValue;
            }
            set
            {
                _baseValue = value;
                Value = _baseValue;

            }
        }
        public int BaseCurrentValuesDifference
        {
            get
            {
                return BaseValue - CurrentValue;
            }
        }

        public override void ApplyItemStat(ItemStat iStat)
        {
            int missingHP = Value - CurrentValue;
            base.ApplyItemStat(iStat);
            CurrentValue = Value - missingHP;
        }

        public override void RemoveItemStat(ItemStat iStat)
        {
            int missingHP = Value - CurrentValue;
            base.RemoveItemStat(iStat);
            CurrentValue = Mathf.Max(Value - missingHP, 1);
        }

        public override string ToString()
        {
            return string.Format("HP: {0}/{1}", CurrentValue.ToString(), Value.ToString());
        }
    }
}