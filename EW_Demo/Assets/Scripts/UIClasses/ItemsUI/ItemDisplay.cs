﻿using UnityEngine;
using System.Collections;
using EW.ItemClasses;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace EW.UIClasses.ItemsUI
{
    public class ItemDisplay : MonoBehaviour/*, IDragHandler, IBeginDragHandler, IEndDragHandler, IDropHandler*/
    {
        public TextMeshProUGUI _tItemName;
        public TextMeshProUGUI _stats;
        public Image _displayImage;

        private Item _item;

        public void DisplayItem(Item item)
        {
            gameObject.SetActive(true);

            if (item == _item)
                return;

            _item = item;

            _tItemName.text = _item.itemName;
            _tItemName.color = _item._itemRarity._color;

            string statsText = "";
            foreach (ItemStat iStat in _item.itemStats)
            {
                if (statsText != "")
                    statsText += "\n";

                statsText += iStat.description;
            }

            _stats.text = statsText;

            _displayImage.preserveAspect = true;
            _displayImage.sprite = _item._displaySprite;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        //private Vector2 lastMousePosition;
        //public void OnBeginDrag(PointerEventData eventData)
        //{
        //    lastMousePosition = eventData.position;
        //}

        //public void OnDrag(PointerEventData eventData)
        //{
        //    Vector2 currentMousePosition = eventData.position;
        //    Vector2 diff = currentMousePosition - lastMousePosition;
        //    RectTransform rect = GetComponent<RectTransform>();

        //    Vector3 newPosition = rect.position + new Vector3(diff.x, diff.y, transform.position.z);
        //    Vector3 oldPos = rect.position;
        //    rect.position = newPosition;
        //    if (!IsRectTransformInsideSreen(rect))
        //    {
        //        rect.position = oldPos;
        //    }
        //    lastMousePosition = currentMousePosition;
        //}

        //private bool IsRectTransformInsideSreen(RectTransform rectTransform)
        //{
        //    bool isInside = false;
        //    Vector3[] corners = new Vector3[4];
        //    rectTransform.GetWorldCorners(corners);
        //    int visibleCorners = 0;
        //    Rect rect = new Rect(0, 0, Screen.width, Screen.height);
        //    foreach (Vector3 corner in corners)
        //    {
        //        if (rect.Contains(corner))
        //        {
        //            visibleCorners++;
        //        }
        //    }
        //    if (visibleCorners == 4)
        //    {
        //        isInside = true;
        //    }
        //    return isInside;
        //}

        //public void OnDrop(PointerEventData eventData)
        //{
        //    RectTransform invPanel = transform as RectTransform;

        //    if (!RectTransformUtility.RectangleContainsScreenPoint(invPanel, Input.mousePosition))
        //    {
        //        //drop item
        //    }
        //}

        //public void OnEndDrag(PointerEventData eventData)
        //{
        //    //throw new System.NotImplementedException();
        //}
    }
}