﻿using UnityEngine;
using System.Collections;
using EW.CharacterClasses.BodyParts;
using UnityEngine.UI;
using EW.UIClasses.ItemsUI;
using EW.ItemClasses;
using System;
using EW.CharacterClasses;

namespace EW.UIClasses.CharacterUI
{
    public class HandStatsDisplay : BodyPartStatsDisplay
    {
        [Header("Hand Display Properties")]
        [SerializeField]
        private ItemInventoryDisplay _weaponSlot;
        public ItemInventoryDisplay WeaponSlot { get => _weaponSlot; }

        public override void Init(BodyPart bPart, Image bpDisplayImage, SelectBodyPart selectBodyPartCallback)
        {
            base.Init(bPart, bpDisplayImage, selectBodyPartCallback);

            Hand h = BodyPart as Hand;
            if(h != null)
            {
                h.OnWeaponEquipped += OnWeaponEquipped;
                h.OnWeaponUnequipped += OnWeaponUnequipped;
            }
        }

        private void OnWeaponEquipped(Item i)
        {
            WeaponSlot.SetDisplayItem(i);
        }

        private void OnWeaponUnequipped()
        {
            WeaponSlot.SetDisplayItem(null);
            SetDisplayHighlight(false);
        }

        
    }
}