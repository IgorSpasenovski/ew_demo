﻿using UnityEngine;
using System.Collections;
using TMPro;
using EW.CharacterClasses.BodyParts;
using EW.CharacterClasses.Stats;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using EW.ItemClasses;
using EW.UIClasses.ItemsUI;
using EW.CharacterClasses;

namespace EW.UIClasses.CharacterUI
{
    public delegate void SelectBodyPart(BodyPart target);

    public class BodyPartStatsDisplay : MonoBehaviour, IDropHandler
    {
        [SerializeField]
        private TextMeshProUGUI _tBodyPartName;
        [SerializeField]
        private TextMeshProUGUI _tHealthPoints;
        [SerializeField]
        private TextMeshProUGUI _tDefense;
        [SerializeField]
        private ItemInventoryDisplay _itemSlot;
        public ItemInventoryDisplay ItemSlot { get => _itemSlot; }

        [Space]
        [SerializeField]
        private Image _background;
        private Image _iBodyPartGraphic;
        [Space]
        [SerializeField]
        private ColorBlock _btnColors;

        public BodyPart BodyPart { get; private set; }

        private bool _selecteAsAttackTarget = false;

        public SelectBodyPart SelectBodyPartCallback;

        [SerializeField]
        private AnimatedStatTextChange _animatedStatChangePrefab;

        public virtual void Init(BodyPart bPart, Image bpDisplayImage, SelectBodyPart selectBodyPartCallback)
        {
            BodyPart = bPart;

            _tBodyPartName.text = bPart.displayName;

            BodyPartDefense bpDefense = bPart.GetStat<BodyPartDefense>() as BodyPartDefense;
            _tDefense.text = bpDefense.ToString();
            bpDefense.OnStatChangeEvent += OnDefenseStatChange;

            HealthPoints hpStat = bPart.GetStat<HealthPoints>() as HealthPoints;
            _tHealthPoints.text = hpStat.ToString();
            hpStat.OnStatChangeEvent += OnHealthPointsStatChange;

            BodyPart.OnItemEquipped += OnItemEquipped;
            BodyPart.OnItemUnequipped += OnItemUnequipped;

            BodyPart.OnSelectedAttackTarget += OnSelectedAttackTarget;
            BodyPart.OnDeselectedAttackTarget += OnDeselectedAttackTarget;

            SelectBodyPartCallback = selectBodyPartCallback;

            gameObject.AddComponent<BodyPartDisplaySelectPair>().Init(OnPointerEnter, OnPointerExit, OnPointerDown);
            _iBodyPartGraphic = bpDisplayImage;
            _iBodyPartGraphic.gameObject.AddComponent<BodyPartDisplaySelectPair>().Init(OnPointerEnter, OnPointerExit, OnPointerDown);
        }

        private void OnDefenseStatChange(string statToString, int changeDifference)
        {
            _tDefense.text = statToString;
            AddAnimatedStatChange(_tDefense, changeDifference);
        }
        private void OnHealthPointsStatChange(string statToString, int changeDifference)
        {
            _tHealthPoints.text = statToString;
            AddAnimatedStatChange(_tHealthPoints, changeDifference);

        }
        private void AddAnimatedStatChange(TextMeshProUGUI statDisplayText, int changeDifference)
        {
            AnimatedStatTextChange animStat = Instantiate(_animatedStatChangePrefab);
            animStat.Init(statDisplayText, changeDifference);
        }
        private void OnItemEquipped(Item i)
        {
            ItemSlot.SetDisplayItem(i);
        }

        private void OnItemUnequipped()
        {
            ItemSlot.SetDisplayItem(null);
            SetDisplayHighlight(false);

        }

        public void OnPointerExit()
        {
            SetDisplayHighlight(false);
        }

        public void OnPointerEnter()
        {
            SetDisplayHighlight(true);
        }

        private void OnPointerDown()
        {
            SelectBodyPartCallback?.Invoke(BodyPart);
        }

        private void OnSelectedAttackTarget()
        {
            _selecteAsAttackTarget = true;
            _background.color = _btnColors.pressedColor;
            _iBodyPartGraphic.color = _btnColors.pressedColor;
        }
        private void OnDeselectedAttackTarget()
        {
            _selecteAsAttackTarget = false;
            SetDisplayHighlight(false);
        }

        public void SetDisplayHighlight(bool highlight)
        {
            if (_selecteAsAttackTarget == false)
            {
                _background.color = highlight ? _btnColors.highlightedColor : _btnColors.normalColor;
                _iBodyPartGraphic.color = highlight ? _btnColors.highlightedColor : _btnColors.normalColor;
            }
        }

        public void OnDrop(PointerEventData eventData)
        {
            //RectTransform invPanel = transform as RectTransform;
            //if (RectTransformUtility.RectangleContainsScreenPoint(invPanel, Input.mousePosition))
            //{

            if (eventData.pointerDrag != null)
            {
                ItemInventoryDisplay invDisp = eventData.pointerDrag.GetComponent<ItemInventoryDisplay>();
                if (invDisp)
                {
                    if (invDisp.CurrentItem)
                    {
                        if (invDisp.CurrentItem.bodyPartType.GetType() == BodyPart.GetType())
                        {
                            if(invDisp.CurrentItem.GetType() == typeof(WeaponItem))
                            {
                                //additional can equip check
                                invDisp.EquipItem();
                            }
                            else
                            {
                                invDisp.EquipItem();
                            }
                        }
                    }
                }
            }
            
        }
    }

    public class BodyPartDisplaySelectPair: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
    {
        public delegate void BodyPartPointerEvent();
        public BodyPartPointerEvent onPointerEnter;
        public BodyPartPointerEvent onPointerExit;
        public BodyPartPointerEvent onPointerDown;
        public void Init(BodyPartPointerEvent pointerEnter, BodyPartPointerEvent pointerExit, BodyPartPointerEvent pointerDown)
        {
            onPointerEnter = pointerEnter;
            onPointerExit = pointerExit;
            onPointerDown = pointerDown;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onPointerExit?.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            onPointerEnter?.Invoke();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            onPointerDown?.Invoke();
        }
    }
}