﻿using EW.ItemClasses;
using UnityEngine;
using UnityEngine.Events;
namespace EW.CharacterClasses.Stats
{
    //[CreateAssetMenu(fileName = "CharacterStat", menuName = "EW_Stats/CharacterStat")]
    public class CharacterStat : ScriptableObject
    {
        public string _displayName;
        [SerializeField]
        private int value = 0;
        public int Value
        {
            get
            {
                return value;
            }
            set
            {
                int changeDifference = value - this.value;
                this.value = value;
                if(changeDifference != 0)
                    OnStatChangeEvent?.Invoke(ToString(), changeDifference);
            }
        }

        protected int _baseValue = 0;
        protected int _percentageModifier = 0;

        public bool _uniqueApplicable = true;
        [SerializeField]
        private bool _hideWhenInactive = false;
        public  bool HideWhenInactive { get => _hideWhenInactive; }
        [SerializeField]
        private bool _negativeStat = false;
        public bool NegativeStat { get => _negativeStat; }


        [SerializeField]
        public ActionActivationPhase[] statActionPhases;
        //try subscribing to each phase event/listener

        public delegate void OnStatChange(string statString, int changeDifference);
        [System.NonSerialized()]
        public OnStatChange OnStatChangeEvent;


        public virtual void ApplyItemStat(ItemStat iStat)
        {
            if (_uniqueApplicable && iStat.IsApplied)
                return;
            
            Value = CalculateTotalStatValue(iStat, true);
            iStat.IsApplied = true;
        }

        public virtual void RemoveItemStat(ItemStat iStat)
        {
            if (_uniqueApplicable && iStat.IsApplied == false)
                return;

            Value = CalculateTotalStatValue(iStat, false);
            iStat.IsApplied = false;

        }
        protected int CalculateTotalStatValue(ItemStat iStat, bool addToValue)
        {
            if (iStat._statModifierType == StatModifierType.BaseValue)
                _baseValue += iStat.statValue * (addToValue?1:-1);
            else if (iStat._statModifierType == StatModifierType.Percentage)
                _percentageModifier += iStat.statValue * (addToValue ? 1 : -1); ;

            int percValue = Mathf.RoundToInt(_baseValue / 100f * _percentageModifier);
            return _baseValue + percValue;
        }
        public override string ToString()
        {
            return _displayName + " : " + Value.ToString();
        }
        public virtual bool IsStatActive()
        {
            return true;
        }
    }

    [System.Serializable]
    public struct ActionActivationPhase
    {
        [ReadOnly]
        public string phaseName;
        public ActionPhase actionPhase;
        public ActionPhaseEvent OnActionPhaseEvent;

        [Header("Activated On Chance %")]
        public bool isActivatedOnChance;
        [Range(0, 100)]
        public int activationChance;

        public bool RunActivationChance()
        {
            if (isActivatedOnChance)
            {
                if (Random.Range(0, 101) > activationChance)//temporary random
                    return false;
            }

            return true;
        }
    }
}