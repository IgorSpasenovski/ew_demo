﻿using UnityEngine;
using System.Collections;
using EW.PlayerClasses;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using EW.DuelCombatClasses;

namespace EW.UIClasses.DuelUI
{
    public class DiceRollDisplay : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField]
        private DiceDisplay _diceDisplayPrefab;

        private List<DiceDisplay> _dices;
        private List<DiceDisplay> Dices
        {
            get
            {
                if(_dices == null)
                    _dices = new List<DiceDisplay>();

                return _dices;
            }
            set
            {
                _dices = value;
            }
        }
        private int _defaultDiceNumber = 2;
        private DiceRoll _diceRollToDisplay;
        private UnityAction OnClickRollDice;

        private IEnumerator _animateDiceRoll;
        public void Init(UnityAction rollDiceAciton)
        {
            OnClickRollDice = rollDiceAciton;
            ResetDiceRollDisplay();
        }

        private void ResetDiceRollDisplay()
        {
            SetDisplayedDiceCount(_defaultDiceNumber);
        }
        private void SetDisplayedDiceCount(int diceCount)
        {
            if (diceCount > Dices.Count)
            {
                int diceDiff = diceCount - Dices.Count;
                for (int i = 0; i < diceDiff; i++)
                {
                    CreateDice();
                }
            }

            for (int i = 0; i < _dices.Count; i++)
            {
                Dices[i].gameObject.SetActive(i < diceCount);
            }
        }
        private void CreateDice()
        {
            DiceDisplay d = Instantiate(_diceDisplayPrefab, transform);
            d.SetDiceValue(1);
            Dices.Add(d);
        }

        public void RollDice(DiceRoll diceRoll)
        {
            _diceRollToDisplay = diceRoll;

            SetDisplayedDiceCount(_diceRollToDisplay.CurrentNumberOfDice);

            if (_animateDiceRoll != null)
                StopCoroutine(_animateDiceRoll);

            _animateDiceRoll = AnimateDiceRoll();
            StartCoroutine(_animateDiceRoll);
        }

        private IEnumerator AnimateDiceRoll()
        {
            float duration = 4f;
            float startTime = Time.time;

            while(Time.time - startTime < duration)
            {

                foreach (DiceDisplay diceDisplay in _dices)
                {
                    if (diceDisplay.gameObject.activeSelf)
                        diceDisplay.SetDiceValue(Random.Range(_diceRollToDisplay._minDiceRoll, _diceRollToDisplay._maxDiceRoll + 1));
                }

                yield return new WaitForSeconds(0.2f);
            }

            for (int i = 0; i < _diceRollToDisplay._currentDiceRoll.Count; i++)
            {
                _dices[i].SetDiceValue(_diceRollToDisplay._currentDiceRoll[i]);
            }

            _animateDiceRoll = null;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnClickRollDice?.Invoke();
        }
    }
}