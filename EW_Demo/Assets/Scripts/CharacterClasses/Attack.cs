﻿
public class Attack
{
    //damage
    //piercing
    public int Damage { get; private set; }
    public bool PiercingAttack { get; private set; }
    public bool CriticalAttack { get; private set; }

    public Attack()
    {
        Damage = 0;
        PiercingAttack = false;
    }
    public void SetAttack(int diceRoll, int attackPower, bool canPierce, bool criticalAttack = false)
    {
        Damage = diceRoll + attackPower;
        CriticalAttack = criticalAttack;
        if (CriticalAttack)
        {
            Damage *= 2;
        }
        PiercingAttack = canPierce;
    }

    public override string ToString()
    {
        return Damage.ToString() + (PiercingAttack ? " (Piercing)" : "") + (CriticalAttack ? " (Critical)" : "");
    }
}
