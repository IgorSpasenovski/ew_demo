﻿using UnityEngine;
using System.Collections;
using EW.DuelCombatClasses;
using EW.CharacterClasses;
using EW.CharacterClasses.BodyParts;
using EW.UIClasses.CharacterUI;
using EW.UIClasses.DuelUI;

namespace EW.PlayerClasses
{
    public class Player
    {
        public string playerName;
        public string PlayerHexColor { get; set; }
        private bool isActivePlayer;
        public bool IsActivePlayer
        {
            get
            {
                return isActivePlayer;
            }

            set
            {
                isActivePlayer = value;
                if(isActivePlayer)
                    PlayerPhase = PlayerTurnPhase.TurnStarted;
            }
        }

        private PlayerTurnPhase _playerPhase;
        public PlayerTurnPhase PlayerPhase { get => _playerPhase; set => _playerPhase = value; }
        private PlayerTurnPhase _pendingPhase;
        public PlayerTurnPhase PendingPhase
        {
            get
            {
                return _pendingPhase;
            }
            set
            {
                _pendingPhase = value;
                OnPendingPhaseChange?.Invoke(_pendingPhase);
            }
        }
        public delegate void PendingPhaseChange(PlayerTurnPhase pendingPhase);
        public PendingPhaseChange OnPendingPhaseChange;

        public bool PlayerActionsEnabled { get { return IsActivePlayer && (PendingPhase == PlayerTurnPhase.PlayerActions); } }
        public bool RollDiceEnabled { get { return IsActivePlayer && (PendingPhase == PlayerTurnPhase.RollDice); } }

        private Character playerCharacter;
        public Character PlayerCharacter { get { return playerCharacter; } }

        public SelectBodyPart SelectBodyPartCallback;

        private BodyPart targetBodyPart;
        public BodyPart TargetBodyPart
        {
            get
            {
                return targetBodyPart;
            }
            private set
            {
                targetBodyPart = value;
            }
        }


        private DiceRoll _diceRoll;
        private DiceRollDisplay _diceRollDisplay;

        public Player(string pName, Character pCharacter, DiceRollDisplay diceRollDisplay)
        {
            playerName = pName;
            playerCharacter = pCharacter;
            playerCharacter.PlayerName = playerName;
            playerCharacter.Player = this;
            _diceRoll = new DiceRoll();
            _diceRollDisplay = diceRollDisplay;
        }

        public void PlayerTestAction()
        {
            if(isActivePlayer)
            {
                PlayerPhase = PendingPhase;
            }
        }

        public void SelectBodyPartTarget(BodyPart target)
        {
            if(PlayerActionsEnabled && target.HealthPoints > 0)
            {
                TargetBodyPart = target;
                TargetBodyPart.OnSelectedAttackTarget?.Invoke();
                PlayerPhase = PendingPhase;
                CombatLog.Instance.AddLog(playerName + "'s attack target is " + TargetBodyPart.displayName, playerName);

            }
        }

        public void UseConsumable()
        {
            if(PlayerActionsEnabled)
            {

            }
        }

        public void RollDice()
        {
            if(RollDiceEnabled)
            {
                //On Dice Roll
                _diceRoll.RollDice(0);
                _diceRollDisplay.RollDice(_diceRoll);
                PlayerPhase = PendingPhase;

            }
        }

        public void ProcessAttack()
        {
            playerCharacter.CalculateTotalAttack(_diceRoll);
        }
        
        public void ProcessDefense(BodyPart attackedBodyPart, Attack incomingAttack)
        {
            playerCharacter.ProcessDefense(attackedBodyPart, incomingAttack);
        }
    }
}