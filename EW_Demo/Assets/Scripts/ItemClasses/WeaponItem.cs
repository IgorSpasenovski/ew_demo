﻿using UnityEngine;
using System.Collections;

namespace EW.ItemClasses
{
    [CreateAssetMenu(fileName = "WeaponItem", menuName ="EW_Items/Weapon")]
    public class WeaponItem : Item
    {
        public WeaponHandType _weaponHandType;
        
    }

    [System.Flags]
    public enum WeaponHandType
    {
        None = 0,
        OneHand = 1,
        OffHand = 2,
        TwoHand = 4//OneHand | OffHand
    }
}