﻿using UnityEngine;
using System.Collections;
namespace EW.ItemClasses
{
    [CreateAssetMenu(fileName = "ItemRarity", menuName = "EW_Items/ItemRarity")]
    public class ItemRarity : ScriptableObject
    {
        public string _name;
        public Color _color;

    }
}