﻿using UnityEngine;
using System.Collections;
using EW.ItemClasses;

namespace EW.CharacterClasses.BodyParts
{
    [CreateAssetMenu(fileName = "Hand", menuName = "EW_BodyParts/Hand")]
    public class Hand : BodyPart
    {
        public string _displayWeaponSlotName;
        public WeaponHandType _weaponHandType;
        private WeaponItem _equippedWeapon;
        public WeaponItem EquippedWeapon { get => _equippedWeapon; private set => _equippedWeapon = value; }
        private Item PreviousWeapon { get; set; }

        public OnItemEquip OnWeaponEquipped;
        public OnItemUneqip OnWeaponUnequipped;


        public override void OnBodyPartDestroyed()
        {
            base.OnBodyPartDestroyed();
            Debug.Log("Destroyed MainHand");
        }

        public bool CanEquipWeapon(WeaponItem w)
        {
            return _weaponHandType.HasFlag(w._weaponHandType);
        }

        public Item EquipWeapon(WeaponItem equipWeapon)
        {
            if (EquippedWeapon != null)
            {
                PreviousWeapon = EquippedWeapon;
                UnequpWeapon();
            }
            EquippedWeapon = equipWeapon;
            EquippedWeapon.IsEquipped = true;

            ApplyEquippedItemStats(EquippedWeapon);
            OnWeaponEquipped?.Invoke(EquippedWeapon);
            return PreviousWeapon;
        }
        public void UnequpWeapon()
        {
            if (EquippedWeapon != null)
            {
                RemoveEquippedItemStats(EquippedWeapon);
                EquippedWeapon.IsEquipped = false;

            }
            EquippedWeapon = null;
            OnWeaponUnequipped?.Invoke();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            if (EquippedWeapon != null)
                EquippedWeapon.ResetItemStats();
        }
    }
}
