﻿using UnityEngine;
using UnityEngine.Events;

public class ActionPhase : ScriptableObject
{
}

[System.Serializable]
public class ActionPhaseEvent : UnityEvent<object, object>
{

}
