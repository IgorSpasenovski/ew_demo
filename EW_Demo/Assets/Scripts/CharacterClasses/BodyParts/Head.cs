﻿using UnityEngine;
using System.Collections;
namespace EW.CharacterClasses.BodyParts
{
    [CreateAssetMenu(fileName = "Head", menuName = "EW_BodyParts/Head")]
    public class Head : BodyPart
    {

        public override void OnBodyPartDestroyed()
        {
            base.OnBodyPartDestroyed();
            Debug.Log("Destroyed Head");
        }
    }
}