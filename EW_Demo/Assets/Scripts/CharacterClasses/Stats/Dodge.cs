﻿using UnityEngine;
using System.Collections;
namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName = "Dodge", menuName = "EW_Stats/Dodge")]
    public class Dodge : CharacterStat
    {
        public bool CanDodge { get; private set; }
        public void OnDefendTryDodge(object o, object p)
        {
            CanDodge = false;

            if (Random.Range(0, 101) < Value)//temporary random
                CanDodge = true;
        }

        public override string ToString()
        {
            return base.ToString() + "%";
        }
    }
}