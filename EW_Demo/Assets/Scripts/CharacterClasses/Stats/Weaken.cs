﻿using UnityEngine;
using System.Collections;
namespace EW.CharacterClasses.Stats
{
    [CreateAssetMenu(fileName = "Weaken", menuName = "EW_Stats/Weaken")]
    public class Weaken : CharacterStat
    {
        [SerializeField]
        private int _damageAmplification = 25;
        [SerializeField]
        private int _initialWeakenValue = 15;
        [SerializeField]
        private int _weakenStepValue = 5;
        public override string ToString()
        {
            return base.ToString() + "%";
        }

        public override bool IsStatActive()
        {
            return Value > 0;
        }

        public int AmplifyDamage(int currentAttack)
        {
            int amplifiedAttack = currentAttack;

            if(IsStatActive())
                amplifiedAttack += Mathf.RoundToInt(currentAttack / 100f * _damageAmplification);

            return amplifiedAttack;
        }

        public void ApplyWeaken(int chanceToApplyWeaken)
        {
            if (Random.Range(0, 101) < chanceToApplyWeaken)
            {
                if (Value < _initialWeakenValue)
                {
                    Value += _initialWeakenValue;
                }
                else
                {
                    Value += _weakenStepValue;
                }
            }
        }
    }
}