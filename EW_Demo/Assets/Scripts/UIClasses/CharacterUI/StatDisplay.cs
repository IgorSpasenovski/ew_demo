﻿using UnityEngine;
using System.Collections;
using TMPro;
using EW.CharacterClasses.Stats;

namespace EW.UIClasses.CharacterUI
{
    public class StatDisplay : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _tStat;
        [SerializeField]
        private AnimatedStatTextChange _animatedStatChangePrefab;

        private CharacterStat _characterStat;
        public void Init(CharacterStat cStat)
        {
            _characterStat = cStat;

            _tStat.text = _characterStat.ToString();

            _characterStat.OnStatChangeEvent += OnStatChangeUpdate;

            CheckIsStatActive();

            if (_characterStat.NegativeStat)
                _tStat.color = Color.red;
        }

        private void OnStatChangeUpdate(string statToString, int changeDifference)
        {
            _tStat.text = statToString;
            AddAnimatedStatChange(_tStat, changeDifference);
            CheckIsStatActive();
        }
        private void AddAnimatedStatChange(TextMeshProUGUI statDisplayText, int changeDifference)
        {
            AnimatedStatTextChange animStat = Instantiate(_animatedStatChangePrefab);
            animStat.Init(statDisplayText, changeDifference);
        }

        private void CheckIsStatActive()
        {
            if (_characterStat.HideWhenInactive)
                gameObject.SetActive(_characterStat.IsStatActive());
        }
    }
}